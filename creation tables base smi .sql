-- 
-- utilisation de la Base de données: `smi`
-- 

CREATE DATABASE smi DEFAULT CHARACTER SET utf8 COLLATE utf8_swedish_ci;
USE smi;
-- --------------------------------------------------------

-- 
-- Structure de la table `connexion`
-- 

CREATE TABLE `connexion` (
  `login` varchar(25) NOT NULL default '',
  `password` varchar(30) NOT NULL default '',
  PRIMARY KEY  (`login`)
) ENGINE=innodb DEFAULT CHARSET=utf8;

-- 
-- Contenu de la table `connexion`
-- 

INSERT INTO `connexion` (`login`, `password`) VALUES ('charlie', 'coco');
INSERT INTO `connexion` (`login`, `password`) VALUES ('isa', 'baba');

-- --------------------------------------------------------

-- 
-- Structure de la table `piece`
-- 

CREATE TABLE `piece` (
  `NumPiece` char(10) NOT NULL default '',
  `LibellePiece` char(25) default NULL,
  `TarifPiece` decimal(9,2) default NULL,
  `CodeType` char(10) default NULL,
  PRIMARY KEY  (`NumPiece`)
) ENGINE=innodb DEFAULT CHARSET=utf8;

-- 
-- Contenu de la table `piece`
-- 

INSERT INTO `piece` (`NumPiece`, `LibellePiece`, `TarifPiece`, `CodeType`) VALUES ('ABIT1', 'ABIT IC7', 194.00, 'CaMe');
INSERT INTO `piece` (`NumPiece`, `LibellePiece`, `TarifPiece`, `CodeType`) VALUES ('ABIT2', 'ABIT NF7-7', 116.00, 'CaMe');
INSERT INTO `piece` (`NumPiece`, `LibellePiece`, `TarifPiece`, `CodeType`) VALUES ('ASUS1', 'ASUSTeK A7N8X', 95.00, 'CaMe');
INSERT INTO `piece` (`NumPiece`, `LibellePiece`, `TarifPiece`, `CodeType`) VALUES ('ASUS', 'ASUSTeK P4G8X', 187.00, 'CaMe');
INSERT INTO `piece` (`NumPiece`, `LibellePiece`, `TarifPiece`, `CodeType`) VALUES ('AMD1', 'AMD Athlon XP 1800+', 58.00, 'Proc');
INSERT INTO `piece` (`NumPiece`, `LibellePiece`, `TarifPiece`, `CodeType`) VALUES ('AMD2', 'AMD Athlon XP2000+', 73.00, 'Proc');
INSERT INTO `piece` (`NumPiece`, `LibellePiece`, `TarifPiece`, `CodeType`) VALUES ('AMD3', 'AMD Athlon XP3200+', 395.00, 'Proc');
INSERT INTO `piece` (`NumPiece`, `LibellePiece`, `TarifPiece`, `CodeType`) VALUES ('INTEL1', 'Intel P4 2.4B Ghz', 184.00, 'Proc');
INSERT INTO `piece` (`NumPiece`, `LibellePiece`, `TarifPiece`, `CodeType`) VALUES ('INTEL2', 'Intel P4 3.06 Ghz HT', 329.00, 'Proc');
INSERT INTO `piece` (`NumPiece`, `LibellePiece`, `TarifPiece`, `CodeType`) VALUES ('INTEL3', 'Intel Celeron 1.7 Ghz', 63.00, 'Proc');
INSERT INTO `piece` (`NumPiece`, `LibellePiece`, `TarifPiece`, `CodeType`) VALUES ('DDR1', '128 Mo DDR SDRAM PC2700', 27.00, 'MeVi');
INSERT INTO `piece` (`NumPiece`, `LibellePiece`, `TarifPiece`, `CodeType`) VALUES ('DDR2', '512 Mo DDR SDRAM PC2700', 46.00, 'MeVi');
INSERT INTO `piece` (`NumPiece`, `LibellePiece`, `TarifPiece`, `CodeType`) VALUES ('DDR3', '512 Mo DDR SDRAM PC3200', 89.00, 'MeVi');
INSERT INTO `piece` (`NumPiece`, `LibellePiece`, `TarifPiece`, `CodeType`) VALUES ('SEAG1', 'Seagate 60 Mo 7200', 88.00, 'DiDu');
INSERT INTO `piece` (`NumPiece`, `LibellePiece`, `TarifPiece`, `CodeType`) VALUES ('SEAG2', 'Seagate 80 Mo 7200', 82.00, 'DiDu');
INSERT INTO `piece` (`NumPiece`, `LibellePiece`, `TarifPiece`, `CodeType`) VALUES ('MAXT1', 'Maxtor 60 Go 7200', 75.00, 'DiDu');
INSERT INTO `piece` (`NumPiece`, `LibellePiece`, `TarifPiece`, `CodeType`) VALUES ('MAXT2', 'Maxtor 80 Go 7200', 78.00, 'DiDu');
INSERT INTO `piece` (`NumPiece`, `LibellePiece`, `TarifPiece`, `CodeType`) VALUES ('MAXT3', 'Maxtor 250 Go 7200', 286.00, 'DiDu');
INSERT INTO `piece` (`NumPiece`, `LibellePiece`, `TarifPiece`, `CodeType`) VALUES ('GEFO1', 'Geforce 4 Ti 4200 64 Mo', 134.00, 'CaGr');
INSERT INTO `piece` (`NumPiece`, `LibellePiece`, `TarifPiece`, `CodeType`) VALUES ('GEFO2', 'GeForce4 MX440 64 Mo', 50.00, 'CaGr');
INSERT INTO `piece` (`NumPiece`, `LibellePiece`, `TarifPiece`, `CodeType`) VALUES ('RADE1', 'Radeon9000 Pro 128 Mo', 79.00, 'CaGr');
INSERT INTO `piece` (`NumPiece`, `LibellePiece`, `TarifPiece`, `CodeType`) VALUES ('RADE2', 'Radeon9800 Pro 128 Mo', 389.00, 'CaGr');
INSERT INTO `piece` (`NumPiece`, `LibellePiece`, `TarifPiece`, `CodeType`) VALUES ('SEAG3', 'Seagate 120 Mo SATA', 127.00, 'DiDu');

-- --------------------------------------------------------

-- 
-- Structure de la table `type`
-- 

CREATE TABLE `type` (
  `CodeType` varchar(10) NOT NULL default '',
  `LibelleType` varchar(25) NOT NULL default '',
  `ImageType` varchar(30) NOT NULL default '',
  PRIMARY KEY  (`CodeType`)
) ENGINE=innodb DEFAULT CHARSET=utf8;

-- 
-- Contenu de la table `type`
-- 

INSERT INTO `type` (`CodeType`, `LibelleType`, `ImageType`) VALUES ('CaMe', 'Carte Mere', '<img src="image\/cm.jpg">');
INSERT INTO `type` (`CodeType`, `LibelleType`, `ImageType`) VALUES ('Proc', 'Processeur', '<img src="image\/prc.jpg">');
INSERT INTO `type` (`CodeType`, `LibelleType`, `ImageType`) VALUES ('CaGr', 'Carte graphique', '<img src="image\/cg.jpg">');
INSERT INTO `type` (`CodeType`, `LibelleType`, `ImageType`) VALUES ('DiDu', 'Disque dur', '<img src="image\/ddur.jpg">');
INSERT INTO `type` (`CodeType`, `LibelleType`, `ImageType`) VALUES ('MeVi', 'Memoire vive', '<img src="image\/cg.jpg">');
        
